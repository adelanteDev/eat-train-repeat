package com.example.adelante.eattrainrape;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Bmi_Whr extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi__whr);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);

        final Button button_obl = (Button) findViewById(R.id.buttonObliczBMI);
        final Button button_whr = (Button) findViewById(R.id.buttonObliczWHR);
        final TextView txtWzrost = (TextView) findViewById(R.id.textWzrostBMI);
        final TextView txtWaga = (TextView) findViewById(R.id.textWagaBMI);
        final TextView txtTalia = (TextView) findViewById(R.id.textTalia);
        final TextView txtBiodra = (TextView) findViewById(R.id.textBiodra);
        final TextView view15 = (TextView) findViewById(R.id.textView15);
        final TextView view12 = (TextView) findViewById(R.id.textView12);
        final TextView view13 = (TextView) findViewById(R.id.textView13);
        final TextView view22 = (TextView) findViewById(R.id.textView22);

        button_obl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String strWzr = txtWzrost.getText().toString();
                String strWag = txtWaga.getText().toString();

                if (TextUtils.isEmpty(strWzr)) {
                    txtWzrost.setError("Uzupełnij wzrost!");
                    return;
                } else if (TextUtils.isEmpty(strWag)) {
                    txtWaga.setError("Uzupełnij wagę!");
                    return;
                } else {
                    Double waga = Double.parseDouble(txtWaga.getText().toString());
                    Double wzrost = Double.parseDouble(txtWzrost.getText().toString());
                    Double bmi = 0.0;

                    bmi = waga / Math.pow(wzrost / 100.0, 2.0);

                    double round = Math.round(bmi * 100.0) / 100.0;

                    view15.setText("BMI : " + round);

                    if (round > 30.0) {
                        view12.setVisibility(v.VISIBLE);
                        view13.setVisibility(v.VISIBLE);
                        view22.setVisibility(v.VISIBLE);
                        txtTalia.setVisibility(v.VISIBLE);
                        txtBiodra.setVisibility(v.VISIBLE);
                        button_whr.setVisibility(v.VISIBLE);
                    }
                }
            }
        });
        button_whr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String strTalia = txtTalia.getText().toString();
                String strBiodra = txtBiodra.getText().toString();
                if (TextUtils.isEmpty(strTalia)) {
                    txtTalia.setError("Uzupełnij Obwód Talii!");
                    return;
                } else if (TextUtils.isEmpty(strBiodra)) {
                    txtBiodra.setError("Uzupełnij Obwód Bioder!");
                    return;
                } else {
                    Double talia = Double.parseDouble(txtTalia.getText().toString());
                    Double biodra = Double.parseDouble(txtBiodra.getText().toString());
                    Double whr;

                    whr = talia / biodra;
                    double round = Math.round(whr * 100.0) / 100.0;

                    if (whr <= 0.84) {
                        view22.setText("WHR : " +round + " (Figura typu gruszka)");

                    }
                    if (whr > 0.84) {
                        view22.setText("WHR : " +round + " (Figura typu jabłko)");
                    }
                }
            }

        });
    }
}