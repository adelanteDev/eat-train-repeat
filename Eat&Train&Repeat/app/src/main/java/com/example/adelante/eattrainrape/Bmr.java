package com.example.adelante.eattrainrape;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

public class Bmr extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmr);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);

        final Button button_OK = (Button) findViewById(R.id.buttonOblicz);
        final TextView view10 = (TextView) findViewById(R.id.textView10);
        final TextView textWzr = (TextView) findViewById(R.id.textWzrost);
        final TextView textWag = (TextView) findViewById(R.id.textWaga);
        final TextView textWie = (TextView) findViewById(R.id.textWiek);
        final TextView txtSila = (TextView) findViewById(R.id.txtSilowy);
        final TextView txtAero = (TextView) findViewById(R.id.txtAerobowy);

        final RadioButton radioMez = (RadioButton) findViewById(R.id.radioM);
        final RadioButton radioEktoo = (RadioButton) findViewById(R.id.radioEkto);
        final RadioButton radioMezoo = (RadioButton) findViewById(R.id.radioMezo);
        final RadioButton radioEndoo = (RadioButton) findViewById(R.id.radioEndo);
        final RadioButton radioPrzyt = (RadioButton) findViewById(R.id.radioPrzytyc);
        final RadioButton radioSchud = (RadioButton) findViewById(R.id.radioSchudnac);
        final RadioButton radioUtrzy = (RadioButton) findViewById(R.id.radioUtrzymac);

        final Spinner spinnerSil = (Spinner) findViewById(R.id.spinnerSilowy);
        final Spinner spinnerAero = (Spinner) findViewById(R.id.spinnerAerobowy);

        button_OK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String strWzr = textWzr.getText().toString();
                String strWag = textWag.getText().toString();
                String strWie = textWie.getText().toString();
                String strSil = txtSila.getText().toString();
                String strAer = txtAero.getText().toString();

                if (TextUtils.isEmpty(strWzr)) {
                    textWzr.setError("Uzupełnij!");
                    return;
                }
                else if (TextUtils.isEmpty(strWag)) {
                    textWag.setError("Uzupełnij! Nawet jeśli jesteś kobietą!");
                    return;
                }
                else if (TextUtils.isEmpty(strWie)) {
                    textWie.setError("Uzupełnij!");
                    return;
                }
                else if (TextUtils.isEmpty(strSil)) {
                    txtSila.setError("Uzupełnij! Jeśli nie ćwiczysz wpisz 0");
                    return;
                }
                else if (TextUtils.isEmpty(strAer)) {
                    txtAero.setError("Uzupełnij! Jeśli nie ćwiczysz wpisz 0");
                    return;
                }
                else {

                    String intenSil = spinnerSil.getSelectedItem().toString();
                    String intenAer = spinnerAero.getSelectedItem().toString();

                    Double wiek = Double.parseDouble(textWie.getText().toString());
                    Double waga = Double.parseDouble(textWag.getText().toString());
                    Double wzrost = Double.parseDouble(textWzr.getText().toString());

                    Double tea = 0.0;
                    Double teaSil = 0.0;
                    Double teaAer = 0.0;
                    Double minAero = 0.0;
                    Double eMe = 0.0;
                    Double kM = 0.0;
                    Double minSil = 0.0;
                    Double teaBmr = 0.0;
                    Double neat = 0.0;
                    Double tef = 0.0;
                    Double cel = 0.0;

                    if (radioMez.isChecked()) {
                        kM = 5.0;
                    } else {
                        kM = -161.0;
                    }
                    if (radioEktoo.isChecked()) {
                        eMe = 800.0;
                    }
                    if (radioMezoo.isChecked()) {
                        eMe = 450.0;
                    }
                    if (radioEndoo.isChecked()) {
                        eMe = 300.0;
                    }

                    Double bmr = (9.99 * waga) + (6.25 * wzrost) - (4.92 * wiek) + kM;

                    if (Double.parseDouble(txtSila.getText().toString()) > 0.0) {
                        minSil = Double.parseDouble(txtSila.getText().toString());
                        if (intenSil.equals("Niska")) {
                            teaSil = 3 * minSil * 7 + (3 * (0.07 * bmr));
                        }
                        if (intenSil.equals("Średnia")) {
                            teaSil = 3 * minSil * 8 + (3 * (0.07 * bmr));
                        }
                        if (intenSil.equals("Wysoka")) {
                            teaSil = 3 * minSil * 9 + (3 * (0.07 * bmr));
                        }

                    } else {
                        teaSil = 0.0;
                    }
                    if (Double.parseDouble(txtAero.getText().toString()) > 0.0) {
                        minAero = Double.parseDouble(txtAero.getText().toString());
                        if (intenAer.equals("Niska")) {
                            teaAer = (3 * minAero * 5) + 5;
                        }
                        if (intenAer.equals("Średnia")) {
                            teaAer = (3 * minAero * 7.5) + 35;
                        }
                        if (intenAer.equals("Wysoka")) {
                            teaAer = (3 * minAero * 10) + 180;
                        }

                    } else {
                        teaAer = 0.0;
                    }

                    if (radioSchud.isChecked()) {
                        cel = -300.0;
                    }
                    if (radioUtrzy.isChecked()) {
                        cel = 0.0;
                    }
                    if (radioPrzyt.isChecked()) {
                        cel = 300.0;
                    }

                    tea = (teaSil + teaAer) / 7.0;

                    teaBmr = bmr + tea;

                    neat = teaBmr + eMe;

                    tef = (neat + (0.1 * neat)) + cel;

                    double roundTef = Math.round(tef * 100.0) / 100.0;


                    view10.setText("Twoje kalorie : " + roundTef);

                }
            }
        });
    }
}