package com.example.adelante.eattrainrape;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class BodyFat extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_fat);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        final RadioButton radioMez = (RadioButton) findViewById(R.id.BFradioM);
        final Button button_OK = (Button) findViewById(R.id.buttonObliczBF);
        final TextView wskaznikTkanki = (TextView) findViewById(R.id.textView17);
        final TextView textObwod = (TextView) findViewById(R.id.textObwodBF);
        final TextView textWaga = (TextView) findViewById(R.id.textWagaBF);

        button_OK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String strObwod = textObwod.getText().toString();
                String strWaga = textWaga.getText().toString();

                if (TextUtils.isEmpty(strWaga)) {
                    textWaga.setError("Uzupełnij!");
                    return;
                }
                else if (TextUtils.isEmpty(strObwod)) {
                    textObwod.setError("Uzupełnij!");
                    return;
                }

                else{

                    Double waga = Double.parseDouble(textWaga.getText().toString());
                    Double obwod = Double.parseDouble(textObwod.getText().toString());

                    Double wsk_tk = 0.0;
                    Double kM = 0.0;
                    Double a,b,c,d,e = 0.0;

                    if (radioMez.isChecked()) {
                        kM = 98.42;
                    } else {
                        kM = 76.76;
                    }


                    a=4.15*obwod;
                    b=a/2.54;
                    c=0.082*waga*2.2;
                    d=b-c-kM;
                    e=waga*2.2;
                    wsk_tk = d/e * 100;

                    wskaznikTkanki.setText("Wskaźnik BF : " + wsk_tk.intValue() + " %");

                }
            }
        });
    }
}
