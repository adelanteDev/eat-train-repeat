package com.example.adelante.eattrainrape;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;


public class DB extends _Default implements Runnable {

    private Connection conn;

    private String url = "";

    public DB()
    {
        super();
        this.url = "jdbc:postgresql://ec2-54-195-252-202.eu-west-1.compute.amazonaws.com:5432/d917k6j7ma3qah?user=spmghbdfanjnvj&password=f_pYDoVOcaihqXS9CK8DllROj9&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";

        this.connect();
        this.disconnect();
    }

    @Override
    public void run()
    {
        try{
            Class.forName("org.postgresql.Driver");
            this.conn = DriverManager.getConnection("jdbc:postgresql://ec2-54-195-252-202.eu-west-1.compute.amazonaws.com:5432/d917k6j7ma3qah?user=spmghbdfanjnvj&password=f_pYDoVOcaihqXS9CK8DllROj9&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory");
        }catch (Exception e){
            this._message = e.getMessage();
            this._status = false;
        }
    }

    private void connect()
    {
        Thread thread = new Thread(this);
        thread.start();
        try{
            thread.join();
        }catch (Exception e){
            this._message = e.getMessage();
            this._status = false;
        }
    }

    private void disconnect()
    {
        if (this.conn!= null){
            try{
                this.conn.close();
            }catch (Exception e){

            }finally {
                this.conn = null;
            }
        }
    }

    public ResultSet select(String query)
    {
        this.connect();
        ResultSet resultSet = null;
        try {
            resultSet = new ExecuteDB(this.conn, query).execute().get();
        }catch (Exception e){
            this._status = false;
            this._message = e.getMessage();
        }
        return resultSet;
    }

    public ResultSet execute(String query){
        this.connect();
        ResultSet resultSet = null;
        try {
            resultSet = new ExecuteDB(this.conn, query).execute().get();
        }catch (Exception e){
            this._status = false;
            this._message = e.getMessage();
        }
        return resultSet;
    }

}