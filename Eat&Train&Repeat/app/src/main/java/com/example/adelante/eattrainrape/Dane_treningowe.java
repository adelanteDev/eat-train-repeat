package com.example.adelante.eattrainrape;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by Marcin on 2015-12-13.
 */


public class Dane_treningowe extends Activity {
    public static final String PREFS_NAME = "Danetreningowe";
    // Stringi przechowywane są w 7 stringach ponizej w formie np. "Klatka" lub jak wiecej "Klatka&Barki".
    static String treningPoniedzialek = "";
    static String treningWtorek = "";
    static String treningSroda = "";
    static String treningCzwartek = "";
    static String treningPiatek = "";
    static String treningSobota = "";
    static String treningNiedziela = "";
    static int choosenDay;

    void setTheTrainingForDay(int i, String buildedString) {
        switch (i) {
            case 0:
                treningPoniedzialek = buildedString;
                break;
            case 1:
                treningWtorek = buildedString;
                break;
            case 2:
                treningSroda = buildedString;
                break;
            case 3:
                treningCzwartek = buildedString;
                break;
            case 4:
                treningPiatek=buildedString;
                break;
            case 5:
                treningSobota=buildedString;
                break;
            case 6:
                treningNiedziela=buildedString;
                break;

        }
    }

    void setTheDay(int i) {
        choosenDay = i;
    }

    int getTheDay() {
        return choosenDay;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dane_treningowe);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        final Button button_treningPoniedzialek = (Button) findViewById(R.id.treningiPoniedzialek);
        final Button button_treningWtorek = (Button) findViewById(R.id.treningiWtorek);
        final Button button_treningSroda = (Button) findViewById(R.id.treningiSroda);
        final Button button_treningCzwartek = (Button) findViewById(R.id.treningiCzwartek);
        final Button button_treningPiatek = (Button) findViewById(R.id.treningiPiatek);
        final Button button_treningSobota = (Button) findViewById(R.id.treningiSobota);
        final Button button_treningNiedziela = (Button) findViewById(R.id.treningiNiedziela);

        SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        button_treningPoniedzialek.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(0);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);

            }
        });
        button_treningWtorek.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(1);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningSroda.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(2);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningCzwartek.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(3);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningPiatek.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(4);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningSobota.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(5);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningNiedziela.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(6);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        final Button button_treningPoniedzialek = (Button) findViewById(R.id.treningiPoniedzialek);
        final Button button_treningWtorek = (Button) findViewById(R.id.treningiWtorek);
        final Button button_treningSroda = (Button) findViewById(R.id.treningiSroda);
        final Button button_treningCzwartek = (Button) findViewById(R.id.treningiCzwartek);
        final Button button_treningPiatek = (Button) findViewById(R.id.treningiPiatek);
        final Button button_treningSobota = (Button) findViewById(R.id.treningiSobota);
        final Button button_treningNiedziela = (Button) findViewById(R.id.treningiNiedziela);
        // dalsze dni
        SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        button_treningPoniedzialek.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(0);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);

            }
        });
        button_treningWtorek.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(1);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningSroda.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(2);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningCzwartek.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(3);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningPiatek.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(4);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningSobota.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(5);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
        button_treningNiedziela.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTheDay(6);
                Intent i = new Intent(
                        Dane_treningowe.this,
                        Dane_treningowe_treningi.class);
                startActivity(i);
            }
        });
    }
}
