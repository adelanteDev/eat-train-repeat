package com.example.adelante.eattrainrape;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.Image;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by Marcin on 2015-12-13.
 */


public class Dane_treningowe_treningi extends Activity {
    Dane_treningowe dane_treningowe = new Dane_treningowe();
    private String stringTreningowy;

    public void dzienText(int jakiDzien){
        final TextView whatday = (TextView) findViewById(R.id.JakiDzien);
        switch (jakiDzien) {
            case 0:
                whatday.setText("Poniedziałek :");
                break;
            case 1:
                whatday.setText("Wtorek :");
                break;
            case 2:
                whatday.setText("Środa :");
                break;
            case 3:
                whatday.setText("Czwartek :");
                break;
            case 4:
                whatday.setText("Piątek :");
                break;
            case 5:
                whatday.setText("Sobota :");
                break;
            case 6:
                whatday.setText("Niedziela :");
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dane_treningowe_treningi);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        final int jakiDzien = dane_treningowe.getTheDay();
        final TextView text = (TextView) findViewById(R.id.textTest);
        final ImageButton save = (ImageButton) findViewById(R.id.imageButtonSaveT);
        final ImageButton reset = (ImageButton) findViewById(R.id.imageButtonResetT);
        final RadioButton radioKlatka = (RadioButton) findViewById(R.id.radioKlatka);
        final RadioButton radioPlecy = (RadioButton) findViewById(R.id.radioPlecy);
        final RadioButton radioBarki = (RadioButton) findViewById(R.id.radioBarki);
        final RadioButton radioBrzuch = (RadioButton) findViewById(R.id.radioBrzuch);
        final RadioButton radioNogi = (RadioButton) findViewById(R.id.radioNogi);
        final RadioButton radioPrzedramie = (RadioButton) findViewById(R.id.radioPrzedramie);
        final RadioButton radioKaptury = (RadioButton) findViewById(R.id.radioKaptury);
        final RadioButton radioBiceps = (RadioButton) findViewById(R.id.radioBiceps);
        final RadioButton radioTriceps = (RadioButton) findViewById(R.id.radioTriceps);
        final ImageButton add = (ImageButton) findViewById(R.id.imageButtonAddT);
        dzienText(jakiDzien);

        reset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                radioKlatka.setChecked(false);
                radioPlecy.setChecked(false);
                radioBarki.setChecked(false);
                radioBrzuch.setChecked(false);
                radioNogi.setChecked(false);
                radioPrzedramie.setChecked(false);
                radioKaptury.setChecked(false);
                radioBiceps.setChecked(false);
                radioTriceps.setChecked(false);

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String stringTreningowy = "";
                if (radioKlatka.isChecked()) {
                    stringTreningowy += "Klatka";
                }
                if (radioPlecy.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += "&Plecy";
                    else stringTreningowy += "Plecy";
                }
                if (radioBarki.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += "&Barki";
                    else stringTreningowy += "Barki";
                }
                if (radioBrzuch.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += "&Brzuch";
                    else stringTreningowy += "Brzuch";
                }
                if (radioNogi.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += "&Nogi";
                    else stringTreningowy += "Nogi";
                }
                if (radioPrzedramie.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += "&Przedramie";
                    else stringTreningowy += "Przedramie";
                }
                if (radioKaptury.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += "&Kaptury";
                    else stringTreningowy += "Kaptury";
                }
                if (radioBiceps.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Biceps";
                    else stringTreningowy += "Biceps";
                }
                if (radioTriceps.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Triceps";
                    else stringTreningowy += "Triceps";
                }
                dane_treningowe.setTheTrainingForDay(jakiDzien, stringTreningowy);
                text.setText("ZAPISANO");
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(Dane_treningowe_treningi.this);
                alert.setTitle("Dodaj ćwiczenie");
                alert.setMessage("Podaj nazwe ćwiczenia");
                // Create EditText for entry
                final EditText input = new EditText(Dane_treningowe_treningi.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                alert.setView(input);
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                stringTreningowy = input.getText().toString();
                                final TreningiLista nazwa = new TreningiLista(stringTreningowy);

                                switch (jakiDzien) {
                                    case 0:
                                        TreningiLista.treningPoniedzialek.add(nazwa);
                                        break;
                                    case 1:
                                        TreningiLista.treningWtorek.add(nazwa);
                                        break;
                                    case 2:
                                        TreningiLista.treningSroda.add(nazwa);
                                        break;
                                    case 3:
                                        TreningiLista.treningCzwartek.add(nazwa);
                                        break;
                                    case 4:
                                        TreningiLista.treningPiatek.add(nazwa);
                                        break;
                                    case 5:
                                        TreningiLista.treningSobota.add(nazwa);
                                        break;
                                    case 6:
                                        TreningiLista.treningNiedziela.add(nazwa);
                                        break;
                                }

                            }

                        }

                );

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        }
                );
                alert.show();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        final int jakiDzien = dane_treningowe.getTheDay();
        final TextView text = (TextView) findViewById(R.id.textTest);
        final ImageButton save = (ImageButton) findViewById(R.id.imageButtonSaveT);
        final ImageButton reset = (ImageButton) findViewById(R.id.imageButtonResetT);
        final RadioButton radioKlatka = (RadioButton) findViewById(R.id.radioKlatka);
        final RadioButton radioPlecy = (RadioButton) findViewById(R.id.radioPlecy);
        final RadioButton radioBarki = (RadioButton) findViewById(R.id.radioBarki);
        final RadioButton radioBrzuch = (RadioButton) findViewById(R.id.radioBrzuch);
        final RadioButton radioNogi = (RadioButton) findViewById(R.id.radioNogi);
        final RadioButton radioPrzedramie = (RadioButton) findViewById(R.id.radioPrzedramie);
        final RadioButton radioKaptury = (RadioButton) findViewById(R.id.radioKaptury);
        final RadioButton radioBiceps = (RadioButton) findViewById(R.id.radioBiceps);
        final RadioButton radioTriceps = (RadioButton) findViewById(R.id.radioTriceps);
        String doParsowania = "";
        dzienText(jakiDzien);

        if (doParsowania.contains("Klatka")) radioKlatka.setChecked(true);
        if (doParsowania.contains("Plecy")) radioPlecy.setChecked(true);
        if (doParsowania.contains("Barki")) radioBarki.setChecked(true);
        if (doParsowania.contains("Brzuch")) radioBrzuch.setChecked(true);
        if (doParsowania.contains("Nogi")) radioNogi.setChecked(true);
        if (doParsowania.contains("Przedramie")) radioPrzedramie.setChecked(true);
        if (doParsowania.contains("Kaptury")) radioKaptury.setChecked(true);
        if (doParsowania.contains("Biceps")) radioBiceps.setChecked(true);
        if (doParsowania.contains("Triceps")) radioTriceps.setChecked(true);
        reset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                radioKlatka.setChecked(false);
                radioPlecy.setChecked(false);
                radioBarki.setChecked(false);
                radioBrzuch.setChecked(false);
                radioNogi.setChecked(false);
                radioPrzedramie.setChecked(false);
                radioKaptury.setChecked(false);
                radioBiceps.setChecked(false);
                radioTriceps.setChecked(false);

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String stringTreningowy = "";
                if (radioKlatka.isChecked()) {
                    stringTreningowy += "Klatka";
                }
                if (radioPlecy.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Plecy";
                    else stringTreningowy += "Plecy";
                }
                if (radioBarki.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Barki";
                    else stringTreningowy += "Barki";
                }
                if (radioBrzuch.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Brzuch";
                    else stringTreningowy += "Brzuch";
                }
                if (radioNogi.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Nogi";
                    else stringTreningowy += "Nogi";
                }
                if (radioPrzedramie.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Przedramie";
                    else stringTreningowy += "Przedramie";
                }
                if (radioKaptury.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Kaptury";
                    else stringTreningowy += "Kaptury";
                }
                if (radioBiceps.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Biceps";
                    else stringTreningowy += "Biceps";
                }
                if (radioTriceps.isChecked()) {
                    if (stringTreningowy.isEmpty() == false)
                        stringTreningowy += ", Triceps";
                    else stringTreningowy += "Triceps";
                }
                dane_treningowe.setTheTrainingForDay(jakiDzien, stringTreningowy);
                text.setText("ZAPISANO");
            }
        });

    }
}
