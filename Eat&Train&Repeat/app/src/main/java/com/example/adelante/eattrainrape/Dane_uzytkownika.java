package com.example.adelante.eattrainrape;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.test.RenamingDelegatingContext;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import java.text.DecimalFormat;
/**
 * Created by Marcin on 2015-12-13.
 */


public class Dane_uzytkownika extends Activity {
    public static final String PREFS_NAME = "Dane";

    static float wzrost;
    static float waga;
    static int plec; // 0 - facet 1 - kobieta
    static float wegleProcent=0;
    static float zapotrzebowanie=0;
    static float bialkoProcent=0;
    static float tluszczProcent=0;
    static int wyswietlone=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dane_uzytkownika);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        final ImageButton save = (ImageButton) findViewById(R.id.imageButtonSave);
        final ImageButton reset = (ImageButton) findViewById(R.id.imageButtonReset);
        final TextView wzrostKonf = (TextView) findViewById(R.id.textWzrostKonf);
        final TextView wagaKonf = (TextView) findViewById(R.id.textWagaKonf);
        final TextView zapotrzebowanieKonf = (TextView) findViewById(R.id.textZapotrzebowanieKonf);
        final TextView wegleProcKonf = (TextView) findViewById(R.id.textWegleProcentKonf);
        final TextView bialkoProcKonf=(TextView) findViewById(R.id.textBialkoProcentKonf);
        final TextView tluszczProcKonf=(TextView) findViewById(R.id.textTluszczeProcentKonf);
        final RadioButton radioButtonMe = (RadioButton) findViewById(R.id.KonfradioMan);
        final RadioButton radioButtonKa = (RadioButton) findViewById(R.id.KonfradioWoman);
        final TextView zapiszane = (TextView) findViewById(R.id.textZapisz);

        SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        int wyswietlone_get = settings.getInt("wyswietlone", 0);
        float tluszcz = settings.getFloat("tluszcz", 0);
        float bialko = settings.getFloat("bialko", 0);
        float wegle = settings.getFloat("wegle", 0);
        float waga_mem = settings.getFloat("waga", 0);
        float wzrost_mem = settings.getFloat("wzrost", 0);
        float kalorie = settings.getFloat("kalorie", 0);
        int plec_mem = settings.getInt("plec", 0);

        if(wyswietlone_get==1){

            tluszczProcKonf.setText(Float.toString(tluszcz));
            bialkoProcKonf.setText(Float.toString(bialko));
            wegleProcKonf.setText(Float.toString(wegle));
            wagaKonf.setText(Float.toString(waga_mem));
            wzrostKonf.setText(Float.toString(wzrost_mem));
            zapotrzebowanieKonf.setText(Float.toString(kalorie));
            if(plec_mem==0) radioButtonMe.setChecked(true);
            else radioButtonKa.setChecked(true);

        }

        else {

            save.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                    if (wzrostKonf.getText().length() == 0 || wagaKonf.getText().length() == 0 || zapotrzebowanieKonf.getText().length() == 0 || wegleProcKonf.getText().length() == 0 || bialkoProcKonf.getText().length() == 0 || tluszczProcKonf.getText().length() == 0) {
                        zapiszane.setTextColor(Color.RED);
                        zapiszane.setText("Pozostawiono puste pole");
                    } else if (Float.parseFloat(tluszczProcKonf.getText().toString()) + Float.parseFloat(bialkoProcKonf.getText().toString()) + Float.parseFloat(wegleProcKonf.getText().toString()) != 100.0) {
                        zapiszane.setTextColor(Color.RED);
                        zapiszane.setText("Suma procent musi dać 100");
                    } else {


                        zapiszane.setTextColor(Color.GREEN);
                        wyswietlone = 1;
                        tluszczProcent = Float.parseFloat(tluszczProcKonf.getText().toString());
                        bialkoProcent = Float.parseFloat(bialkoProcKonf.getText().toString());
                        wegleProcent = Float.parseFloat(wegleProcKonf.getText().toString());
                        zapotrzebowanie = Float.parseFloat(zapotrzebowanieKonf.getText().toString());
                        wzrost = Float.parseFloat(wzrostKonf.getText().toString());
                        waga = Float.parseFloat(wagaKonf.getText().toString());
                        if (radioButtonKa.isChecked()) plec = 1;
                        else {
                            radioButtonMe.setChecked(true);
                            plec = 0;
                        }
                        zapiszane.setText("SAVED");
                    }

                    SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
                    SharedPreferences.Editor editor = settings.edit();
                        editor.putInt("wyswietlone", wyswietlone);
                        editor.putFloat("tluszcz", tluszczProcent);
                        editor.putFloat("wegle", wegleProcent);
                        editor.putFloat("bialko", bialkoProcent);
                        editor.putFloat("waga", waga);
                        editor.putFloat("wzrost", wzrost);
                        editor.putFloat("kalorie", zapotrzebowanie);
                        editor.putInt("plec", plec);

                    editor.apply();
                }
            });
        }
        reset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final ImageButton save = (ImageButton) findViewById(R.id.imageButtonSave);
                final TextView wzrostKonf = (TextView) findViewById(R.id.textWzrostKonf);
                final TextView wagaKonf = (TextView) findViewById(R.id.textWagaKonf);
                final TextView zapotrzebowanieKonf = (TextView) findViewById(R.id.textZapotrzebowanieKonf);
                final TextView wegleProcKonf = (TextView) findViewById(R.id.textWegleProcentKonf);
                final TextView bialkoProcKonf=(TextView) findViewById(R.id.textBialkoProcentKonf);
                final TextView tluszczProcKonf=(TextView) findViewById(R.id.textTluszczeProcentKonf);
                final RadioButton radioButtonMe = (RadioButton) findViewById(R.id.KonfradioMan);
                final RadioButton radioButtonKa = (RadioButton) findViewById(R.id.KonfradioWoman);

                wyswietlone = 0;
                tluszczProcent = 0;
                wegleProcent =0;
                bialkoProcent =0;
                waga=0;
                wzrost=0;
                zapotrzebowanie=0;
                plec=0;

                SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("wyswietlone", wyswietlone);
                editor.putFloat("tluszcz", tluszczProcent);
                editor.putFloat("wegle", wegleProcent);
                editor.putFloat("bialko", bialkoProcent);
                editor.putFloat("waga", waga);
                editor.putFloat("wzrost", wzrost);
                editor.putFloat("kalorie", zapotrzebowanie);
                editor.putInt("plec", plec);

                editor.apply();

                    wzrostKonf.setText(null);
                    wagaKonf.setText(null);
                    zapotrzebowanieKonf.setText(null);
                    wegleProcKonf.setText(null);
                    tluszczProcKonf.setText(null);
                    bialkoProcKonf.setText(null);
                    radioButtonMe.setChecked(true);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        final ImageButton save = (ImageButton) findViewById(R.id.imageButtonSave);
        final TextView wzrostKonf = (TextView) findViewById(R.id.textWzrostKonf);
        final TextView wagaKonf = (TextView) findViewById(R.id.textWagaKonf);
        final TextView zapotrzebowanieKonf = (TextView) findViewById(R.id.textZapotrzebowanieKonf);
        final TextView wegleProcKonf = (TextView) findViewById(R.id.textWegleProcentKonf);
        final TextView bialkoProcKonf=(TextView) findViewById(R.id.textBialkoProcentKonf);
        final TextView tluszczProcKonf=(TextView) findViewById(R.id.textTluszczeProcentKonf);
        final RadioButton radioButtonMe = (RadioButton) findViewById(R.id.KonfradioMan);
        final RadioButton radioButtonKa = (RadioButton) findViewById(R.id.KonfradioWoman);

        SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        int wyswietlone_get = settings.getInt("wyswietlone", 0);
        float tluszcz = settings.getFloat("tluszcz", 0);
        float bialko = settings.getFloat("bialko", 0);
        float wegle = settings.getFloat("wegle", 0);
        float waga_mem = settings.getFloat("waga", 0);
        float wzrost_mem = settings.getFloat("wzrost", 0);
        float kalorie = settings.getFloat("kalorie", 0);
        int plec_mem = settings.getInt("plec", 0);

        if(wyswietlone_get==1) {
            wzrostKonf.setText(Float.toString(wzrost_mem));
            wagaKonf.setText(Float.toString(waga_mem));
            zapotrzebowanieKonf.setText(Float.toString(kalorie));
            wegleProcKonf.setText(Float.toString(wegle));
            tluszczProcKonf.setText(Float.toString(tluszcz));
            bialkoProcKonf.setText(Float.toString(bialko));
            if(plec_mem==0) radioButtonMe.setChecked(true);
            else radioButtonKa.setChecked(true);
        }
    }
}
