package com.example.adelante.eattrainrape;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.txusballesteros.widgets.FitChart;
import java.text.DecimalFormat;



public class Dieta extends Fragment {

    public float kcal = Float.parseFloat(Food.kalorieDiagram);
    public float fat = Float.parseFloat(Food.tluszczeDiagram);
    public float carbo = Float.parseFloat(Food.wegleDiagram);
    public float prot = Float.parseFloat(Food.bialkoDiagram);

    public static final String PREFS_NAME = "Dane";

/*

    public double maxkcal = Dane_uzytkownika.zapotrzebowanie;
    public double maxwegle = (Dane_uzytkownika.wegleProcent/100)*maxkcal;
    public double maxtluszcze = (Dane_uzytkownika.tluszczProcent/100)*maxkcal;
    public double maxbialko = (Dane_uzytkownika.bialkoProcent/100)*maxkcal; */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        final View view = inflater.inflate(R.layout.dieta_main, container, false);

        final Button button_lista = (Button) view.findViewById(R.id.buttonLista);
        final Button button_dodaj = (Button) view.findViewById(R.id.buttonDodajPosilek);


        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences(PREFS_NAME, 0);

        float tluszcz_mem = settings.getFloat("tluszcz", 0);
        float bialko_mem = settings.getFloat("bialko", 0);
        float wegle_mem = settings.getFloat("wegle", 0);
        float kalorie_mem = settings.getFloat("kalorie", 0);

        double maxkcal = kalorie_mem;
        double maxwegle = (wegle_mem/100)*maxkcal;
        double maxtluszcze = (tluszcz_mem/100)*maxkcal;
        double maxbialko = (bialko_mem/100)*maxkcal;

        button_lista.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), Zjedzone.class);
                startActivity(myIntent);

            }
        });

        button_dodaj.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getContext(), "Ładowanie listy...", Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(getActivity(), ListaActivity.class);
                startActivity(myIntent);
            }
        });

        final FitChart fitChartKalorie = (FitChart)view.findViewById(R.id.fitChartKalorie);

        fitChartKalorie.setMinValue(0f);
        fitChartKalorie.setMaxValue((float)maxkcal);
        fitChartKalorie.setValue(kcal);

        double zjedzoneKalorie = kcal;
        double ustaloneKalorie = 3000f;

        double zjedzoneKalorie2 = Math.floor(zjedzoneKalorie);
        double ustaloneKalorie2 = Math.floor(ustaloneKalorie);

        final TextView kalorie = (TextView) view.findViewById(R.id.kcalCharIle);
        kalorie.setText((int)zjedzoneKalorie2 +  "/" + (int)ustaloneKalorie2);

        final FitChart fitChartFat = (FitChart)view.findViewById(R.id.fitChartFat);
        fitChartFat.setMinValue(0f);
        fitChartFat.setMaxValue((float)maxtluszcze/9);
        fitChartFat.setValue(fat);


        double zjedzoneTluszcze = fat;
        double ustaloneTluszcze = 120f;

        double zjedzoneTluszcze2 = Math.floor(zjedzoneTluszcze);
        double ustaloneTluszcze2 = Math.floor(ustaloneTluszcze);

        final TextView tluszcze = (TextView) view.findViewById(R.id.FatCharIle);
        tluszcze.setText((int) zjedzoneTluszcze2 + "/" + (int) ustaloneTluszcze2);


        final FitChart fitChartProt = (FitChart)view.findViewById(R.id.fitChartProt);
        fitChartProt.setMinValue(0f);
        fitChartProt.setMaxValue((float)maxbialko/4);
        fitChartProt.setValue(prot);

        double zjedzoneBialko = prot;
        double ustaloneBialko = 200f;

        double zjedzoneBialko2 = Math.floor(zjedzoneBialko);
        double ustaloneBialko2 = Math.floor(ustaloneBialko);

        final TextView bialko = (TextView) view.findViewById(R.id.ProteinCharIle);
        bialko.setText((int) zjedzoneBialko2 + "/" + (int) ustaloneBialko2);


        final FitChart fitChartCarbo = (FitChart)view.findViewById(R.id.fitChartCarbo);
        fitChartCarbo.setMinValue(0f);
        fitChartCarbo.setMaxValue((float)maxwegle/4);
        fitChartCarbo.setValue(carbo);

        double zjedzoneWegle = carbo;
        double ustaloneWegle = 300f;

        double zjedzoneWegle2 = Math.floor(zjedzoneWegle);
        double ustaloneWegle2 = Math.floor(ustaloneWegle);

        final TextView wegle = (TextView) view.findViewById(R.id.CarboCharIle);
        wegle.setText((int) zjedzoneWegle2 + "/" + (int) ustaloneWegle2);


        return view;
    }
    @Override
    public void onResume() {

        super.onResume();

        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences(PREFS_NAME, 0);

        float tluszcz_mem = settings.getFloat("tluszcz", 0);
        float bialko_mem = settings.getFloat("bialko", 0);
        float wegle_mem = settings.getFloat("wegle", 0);
        float kalorie_mem = settings.getFloat("kalorie", 0);

        double maxkcal = kalorie_mem;
        double maxwegle = (wegle_mem/100)*maxkcal;
        double maxtluszcze = (tluszcz_mem/100)*maxkcal;
        double maxbialko = (bialko_mem/100)*maxkcal;

       /* double maxkcal = Dane_uzytkownika.zapotrzebowanie;
        double maxwegle = (Dane_uzytkownika.wegleProcent/100)*maxkcal;
        double maxtluszcze = (Dane_uzytkownika.tluszczProcent/100)*maxkcal;
        double maxbialko = (Dane_uzytkownika.bialkoProcent/100)*maxkcal;*/

        DecimalFormat df = new DecimalFormat("#.#");
        final FitChart x = (FitChart)getView().findViewById(R.id.fitChartKalorie);
        final TextView kalorie = (TextView) getView().findViewById(R.id.kcalCharIle);
        float ustaloneKalorie = (float) maxkcal;
        kalorie.setText(df.format(Float.parseFloat(Food.kalorieDiagram)) + "/" + (int)ustaloneKalorie);
        x.setMaxValue((float) maxkcal);
        x.setValue(Float.parseFloat(Food.kalorieDiagram));


        final FitChart y = (FitChart)getView().findViewById(R.id.fitChartCarbo);
        final TextView wegle = (TextView) getView().findViewById(R.id.CarboCharIle);
        float ustaloneWegle = (float)maxwegle;
        wegle.setText(df.format(Float.parseFloat(Food.wegleDiagram)) + "/" + (int) ustaloneWegle / 4);
        y.setValue(Float.parseFloat(Food.wegleDiagram));
        y.setMaxValue((float) maxwegle / 4);

        final FitChart w = (FitChart)getView().findViewById(R.id.fitChartProt);
        final TextView bialko = (TextView) getView().findViewById(R.id.ProteinCharIle);
        float ustaloneBialko = (float) maxbialko;
        bialko.setText(df.format(Float.parseFloat(Food.bialkoDiagram)) + "/" + (int)ustaloneBialko/4);
        w.setValue(Float.parseFloat(Food.bialkoDiagram));
        w.setMaxValue((float) maxbialko/4);

        final FitChart z = (FitChart)getView().findViewById(R.id.fitChartFat);
        final TextView tluszcze = (TextView) getView().findViewById(R.id.FatCharIle);
        float ustaloneTluszcze = (float) maxtluszcze;
        tluszcze.setText(df.format(Float.parseFloat(Food.tluszczeDiagram)) + "/" + (int) ustaloneTluszcze/9);
        z.setValue(Float.parseFloat(Food.tluszczeDiagram));
        z.setMaxValue((float)maxtluszcze/9);



    }



}
