package com.example.adelante.eattrainrape;

import java.sql.ResultSet;
import java.util.ArrayList;

public class Food extends _Default {

    private int id;
    private String names;
    private String calories;
    private String carbs;
    private String proteins;
    private String sugar;
    private String fat;
    private String kind;
    private String barcode;
    static String kalorieDiagram = "0";
    static String bialkoDiagram = "0";
    static String wegleDiagram = "0";
    static String tluszczeDiagram = "0";
    static ArrayList<Food> sniadanieLista = new ArrayList();
    static ArrayList<Food> obiadLista = new ArrayList();
    static ArrayList<Food> kolacjaLista = new ArrayList();

    public Food()
    {
        super();
        this.id = -1;
        this.names = "";
        this.calories = "";
        this.carbs = "";
        this.proteins = "";
        this.sugar = "";
        this.fat = "";
        this.kind =  "";
        this.barcode = "";
    }

    public ArrayList<Food> getLista(){
        DB db = new DB();
        ArrayList<Food> lista = new ArrayList<>();
        try {
            ResultSet resultSet = db.select("SELECT * FROM foods");
            if (resultSet != null){
                while (resultSet.next()){
                    Food obj = new Food();
                    obj.setId(resultSet.getInt("id"));
                    obj.setNames(resultSet.getString("nam"));
                    obj.setCalories(resultSet.getString("calories"));
                    obj.setCarbs(resultSet.getString("carbs"));
                    obj.setProteins(resultSet.getString("proteins"));
                    obj.setSugar(resultSet.getString("sugar"));
                    obj.setFat(resultSet.getString("fat"));
                    obj.setKind(resultSet.getString("kind"));
                    obj.setBarcode(resultSet.getString("barcode"));
                    lista.add(obj);
                    obj = null;
                }
            }
        }catch (Exception ex){
            this._message = ex.getMessage();
            this._status = false;
        }
        return lista;
    }

    public ArrayList<Food> getLista(String kod){
        DB db = new DB();
        ArrayList<Food> lista = new ArrayList<>();
        try {
            ResultSet resultSet = db.select("SELECT * FROM foods WHERE barcode = '" + kod + "' ;");
            if (resultSet != null){
                while (resultSet.next()){
                    Food obj = new Food();
                    obj.setId(resultSet.getInt("id"));
                    obj.setNames(resultSet.getString("nam"));
                    obj.setCalories(resultSet.getString("calories"));
                    obj.setCarbs(resultSet.getString("carbs"));
                    obj.setProteins(resultSet.getString("proteins"));
                    obj.setSugar(resultSet.getString("sugar"));
                    obj.setFat(resultSet.getString("fat"));
                    obj.setKind(resultSet.getString("kind"));
                    obj.setBarcode(resultSet.getString("barcode"));
                    lista.add(obj);
                    obj = null;
                }
            }
        }catch (Exception ex){
            this._message = ex.getMessage();
            this._status = false;
        }
        return lista;
    }
    public ArrayList<Food> getSniadanieLista() {
        return sniadanieLista;
    }
    public ArrayList<Food> getObiadLista() {
        return obiadLista;
    }
    public ArrayList<Food> getKolacjaLista() {
        return kolacjaLista;
    }

    public void update()
    {
        String cmd = "";
        if (this.getId() == -1){
            /*cmd = String.format("INSERT INTO foods (nam, calories, carbs, proteins, sugar, fat, kind, barcode) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s');",
                    this.getNames(),this.getCalories(),this.getCarbs(),this.getProteins(),this.getSugar(),this.getFat(),this,getKind(),this.getBarcode());*/
            cmd = String.format("INSERT INTO foods (nam, calories, carbs, proteins, sugar, fat, kind, barcode) VALUES ('"+getNames()+"','"+getCalories()+"','"+getCarbs()+"','"+getProteins()+"','"+getSugar()+"','"+getFat()+"','"+getKind()+"','"+getBarcode()+"');");
        }else{
            cmd = String.format("UPDATE foods SET nam = '%s', calories = '%s', carbs = '%s', proteins = '%s', sugar = '%s', fat = '%s', kind = '%s', barcode = '%s' WHERE id = %d;",
                    this.getNames(),this.getCalories(),this.getCarbs(),this.getProteins(),this.getSugar(),this.getFat(),this.getKind(),this.getBarcode(),this.getId());
        }
        DB db = new DB();
        db.execute(cmd);

        this._message = db._message;
        this._status = db._status;
    }

    public void delete()
    {
        String cmd = String.format("DELETE FROM foods WHERE id = %d;",this.getId());
        DB db = new DB();
        db.execute(cmd);
        this._message = db._message;
        this._status = db._status;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getCarbs() {
        return carbs;
    }

    public void setCarbs(String carbs) {
        this.carbs = carbs;
    }

    public String getProteins() {
        return proteins;
    }

    public void setProteins(String proteins) {
        this.proteins = proteins;
    }

    public String getSugar() {
        return sugar;
    }

    public void setSugar(String sugar) {
        this.sugar = sugar;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }


}
