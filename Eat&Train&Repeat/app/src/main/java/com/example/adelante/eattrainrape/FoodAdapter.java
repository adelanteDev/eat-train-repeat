package com.example.adelante.eattrainrape;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;


public class FoodAdapter extends ArrayAdapter<Food> {
    private Context context;
    private ArrayList<Food> lista;
    public FoodAdapter(Context context, ArrayList<Food> lista){
        super(context,0,lista);
        this.context = context;
        this.lista = lista;
    }
    private void rodzajJedzenia(final Food zjedzone) {
        final String[] pory = new String[3];
        pory[0] = "Śniadanie";
        pory[1] = "Obiad";
        pory[2] = "Kolacja";
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Wybierz rodzaj posiłku");
        builder.setItems(pory, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (pory[which].equals("Śniadanie")) {
                    Food.sniadanieLista.add(zjedzone);
                }else
                if (pory[which].equals("Obiad")) {
                    Food.obiadLista.add(zjedzone);
                }else
                if(pory[which].equals("Kolacja")){
                    Food.kolacjaLista.add(zjedzone);
                }
                Toast.makeText(getContext(), "Dodano " +zjedzone.getNames() , Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Food itemPosition = this.lista.get(position);

        convertView = LayoutInflater.from(this.context).inflate(R.layout.item_lista,null);
        final View layout = convertView;

        TextView textView = (TextView) convertView.findViewById(R.id.textViewNam);
        textView.setText(itemPosition.getNames());

        TextView textViewCalories = (TextView) convertView.findViewById(R.id.textViewCalories);
        textViewCalories.setText(itemPosition.getCalories()+" kcal");

        TextView textViewCarbs = (TextView) convertView.findViewById(R.id.textViewCarbs);
        textViewCarbs.setText("W: "+itemPosition.getCarbs());

        TextView textViewProteins = (TextView) convertView.findViewById(R.id.textViewProteins);
        textViewProteins.setText("  P: "+itemPosition.getProteins());

        TextView textViewFats = (TextView) convertView.findViewById(R.id.textViewFats);
        textViewFats.setText("  F: " + itemPosition.getFat());

        Button button = (Button)convertView.findViewById(R.id.buttonEdit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewFoodActivity.class);
                intent.putExtra("carbs", itemPosition.getCarbs());
                intent.putExtra("nam", itemPosition.getNames());
                intent.putExtra("calories", itemPosition.getCalories());
                intent.putExtra("id", itemPosition.getId());
                context.startActivity(intent);
            }
        });

        Button buttonDelete = (Button)convertView.findViewById(R.id.buttonDelete);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemPosition.delete();
                if (itemPosition._status)
                    layout.setVisibility(View.GONE);
                else
                    Toast.makeText(context, itemPosition.get_message(), Toast.LENGTH_LONG).show();
            }
        });

        Button buttonAdd = (Button)convertView.findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Dodaj posiłek");
                alert.setMessage("Podaj ilość zjedzonych gramów");
                // Create EditText for entry
                final EditText input = new EditText(context);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setRawInputType(Configuration.KEYBOARD_12KEY);
                alert.setView(input);


                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        String inputGram = input.getText().toString();
                        float Gram = Float.parseFloat(inputGram);
                        float mnoznikGram = Gram / 100;

                        String actualCalories = Food.kalorieDiagram;
                        String actualCarbs = Food.wegleDiagram;
                        String actualProteins = Food.bialkoDiagram;
                        String actualFats = Food.tluszczeDiagram;

                        String addCalories = itemPosition.getCalories();
                        String addCarbs = itemPosition.getCarbs();
                        String addProteins = itemPosition.getProteins();
                        String addFats = itemPosition.getFat();

                        float actual_calories = Float.parseFloat(actualCalories);
                        float actual_carbs = Float.parseFloat(actualCarbs);
                        float actual_proteins = Float.parseFloat(actualProteins);
                        float actual_fats = Float.parseFloat(actualFats);

                        float new_calories = Float.parseFloat(addCalories) * mnoznikGram;
                        float new_carbs = Float.parseFloat(addCarbs) * mnoznikGram;
                        float new_proteins = Float.parseFloat(addProteins) * mnoznikGram;
                        float new_fats = Float.parseFloat(addFats) * mnoznikGram;

                        float result = actual_calories + new_calories;
                        float result_carbs = actual_carbs + new_carbs;
                        float result_proteins = actual_proteins + new_proteins;
                        float result_fats = actual_fats + new_fats;

                        Food.kalorieDiagram = Float.toString(result);
                        Food.wegleDiagram = Float.toString(result_carbs);
                        Food.bialkoDiagram = Float.toString(result_proteins);
                        Food.tluszczeDiagram = Float.toString(result_fats);

                        Food foodZjedzone = new Food();
                        foodZjedzone.setNames(itemPosition.getNames());
                        foodZjedzone.setCalories(Float.toString(new_calories));
                        foodZjedzone.setProteins(Float.toString(new_proteins));
                        foodZjedzone.setCarbs(Float.toString(new_carbs));
                        foodZjedzone.setFat(Float.toString(new_fats));
                        rodzajJedzenia(foodZjedzone);

                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });

                alert.show();


            }
        });

        return convertView;
    }

}
