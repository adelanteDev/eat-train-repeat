package com.example.adelante.eattrainrape;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import java.text.DecimalFormat;

public class IdealnaWaga extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idealna_waga);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        final Button button_Oblicz = (Button) findViewById(R.id.buttonObliczIdealnaWaga);
        final TextView wzrostText = (TextView) findViewById(R.id.textWzrost);
        final TextView lorenzText = (TextView) findViewById(R.id.txtView29);
        final TextView pottonText = (TextView) findViewById(R.id.txtView31);
        final TextView brocText = (TextView) findViewById(R.id.txtView30);
        final RadioButton radioButtonM = (RadioButton) findViewById(R.id.radioButton2);
        final RadioButton radioButtonK = (RadioButton) findViewById(R.id.radioButton1);
        radioButtonK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                radioButtonM.setChecked(false);
                radioButtonK.setChecked(true);
            }
        });

        radioButtonM.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                radioButtonK.setChecked(false);
                radioButtonM.setChecked(true);
            }
        });
        button_Oblicz.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String strWzrost = wzrostText.getText().toString();

                if (TextUtils.isEmpty(strWzrost)) {
                    wzrostText.setError("Uzupełnij!");
                    return;
                } else if (Double.parseDouble(strWzrost) < 0.0 || Double.parseDouble(strWzrost) > 250) {
                    wzrostText.setError("Wpisana wartość wychodzi poza zakres!");
                    return;
                } else {
                    Double wzrost = Double.parseDouble(wzrostText.getText().toString());
                    Double idealnawagaL = 0.0;
                    Double idealnawagaB = 0.0;
                    Double idealnawagaP= 0.0;
                    Double maxWaga=0.0;
                    Double minWaga=0.0;
                    if(radioButtonK.isChecked())
                    {
                        idealnawagaB=(wzrost-100)*0.85;
                        idealnawagaL=wzrost-100-0.25*(wzrost-150);
                        idealnawagaP=(wzrost-100)-((wzrost-100)/10);
                    }
                    else
                    {
                        idealnawagaB=(wzrost-100)*0.9;
                        idealnawagaL=wzrost-100-0.25*(wzrost-150);
                        idealnawagaP=(wzrost-100)-((wzrost-100)/20);
                    }
                    DecimalFormat df = new DecimalFormat("#.##");

                    lorenzText.setText("Waga wg Lorentza : " + df.format(idealnawagaL) + " kg");
                    brocText.setText("Waga wg Broca : " + df.format(idealnawagaB)+ " kg");
                    pottonText.setText("Waga wg Pottona : " + df.format(idealnawagaP)+ " kg");
                }
            }
        });
    }
}