package com.example.adelante.eattrainrape;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class Kalkulator extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.kalkulatory_main, container, false);
        final Button button_BMR = (Button) view.findViewById(R.id.buttonBMR);
        final Button button_BMI = (Button) view.findViewById(R.id.btnBMI_WHR);
        final Button button_Spalanie = (Button) view.findViewById(R.id.btnSpalanie);
        final Button button_BF = (Button) view.findViewById(R.id.btnBF);
        final Button button_Wyciskanie = (Button) view.findViewById(R.id.btn_Wyciskanie);
        final Button button_IdealnaWaga = (Button) view.findViewById(R.id.btn_IdealnaWaga);



        button_BMR.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), Bmr.class);

                startActivity(myIntent);

            }
        });
        button_BMI.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), Bmi_Whr.class);


                startActivity(myIntent);

            }
        });
        button_Spalanie.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), Spalanie.class);


                startActivity(myIntent);

            }
        });

        button_BF.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), BodyFat.class);


                startActivity(myIntent);

            }
        });
        button_Wyciskanie.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), Wyciskanie.class);


                startActivity(myIntent);

            }
        });
        button_IdealnaWaga.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), IdealnaWaga.class);


                startActivity(myIntent);

            }
        });
        return view;
}
}
