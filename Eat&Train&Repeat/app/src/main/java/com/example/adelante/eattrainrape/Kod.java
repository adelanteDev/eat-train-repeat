package com.example.adelante.eattrainrape;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.sql.ResultSet;
import java.sql.SQLException;


public class Kod extends AppCompatActivity {

    private ListView listViewFood;
    String kod = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kod);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);


        Intent intent = getIntent();
        if (intent != null){
            Bundle bundle = intent.getExtras();
            if (bundle != null){
                kod = bundle.getString("barcode");

            }
        }

        this.listViewFood = (ListView) findViewById(R.id.listViewFood);
        this.listViewFood.setAdapter(new FoodAdapter(this, new Food().getLista(kod)));

        DB db = new DB();
        int count = 0;
        ResultSet resultSet = db.select("SELECT * FROM foods WHERE barcode = '" + kod + "' ;");
        if (resultSet != null){
            try {
                while (resultSet.next()){
                    count++;
                }
                if (count == 0)
                {
                showDialog();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_lista, menu);
        return true;
    }


    public void showDialog()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Nie znaleziono!");
        alert.setMessage("Czy chcesz dodać posiłek?");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intent1 = new Intent(getApplication(),NewFoodActivity.class);
                intent1.putExtra("barcode",kod);
                startActivity(intent1);
                finish();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

