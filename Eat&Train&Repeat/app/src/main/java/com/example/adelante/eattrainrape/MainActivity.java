package com.example.adelante.eattrainrape;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "Dane";
    static int odwiedziny=0;
    static int odwiedziny_get = 0;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    Toolbar toolbar;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("odwiedziny", odwiedziny);
        editor.apply();

        odwiedziny_get = settings.getInt("odwiedziny", 0);


        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),toolbar);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    if(odwiedziny_get==0) {
                        toolbar.setBackground(new ColorDrawable(Color.parseColor("#00ff00")));
                        tabLayout.setBackground(new ColorDrawable(Color.parseColor("#00cc00")));
                    }
                    else{
                        toolbar.setBackground(new ColorDrawable(Color.parseColor("#008800")));
                        tabLayout.setBackground(new ColorDrawable(Color.parseColor("#00cc00")));
                    }
                } else if (tab.getPosition() == 1) {
                    toolbar.setBackground(new ColorDrawable(Color.parseColor("#e60000")));
                    tabLayout.setBackground(new ColorDrawable(Color.parseColor("#cc0000")));
                } else if (tab.getPosition() == 2) {
                    toolbar.setBackground(new ColorDrawable(Color.parseColor("#242424")));
                    tabLayout.setBackground(new ColorDrawable(Color.parseColor("#000000")));
                }
                SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                odwiedziny++;
                editor.putInt("odwiedziny", odwiedziny);
                editor.apply();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if(odwiedziny == 0)
        {
            AlertDialog.Builder alertadd = new AlertDialog.Builder(
                    MainActivity.this);
            LayoutInflater factory = LayoutInflater.from(MainActivity.this);
            final View view = factory.inflate(R.layout.first_dialog, null);
            alertadd.setView(view);
            alertadd.setNeutralButton("Ok!", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dlg, int sumthin) {

                }
            });

            alertadd.show();
        }
        odwiedziny++;
        editor.putInt("odwiedziny", odwiedziny);
        editor.apply();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,Dane_uzytkownika.class);
            startActivity(intent);
        }
        else if(id == R.id.action_training_settings)
        {
            Intent intent = new Intent(this,Dane_treningowe.class);
            startActivity(intent);
        }
        else if(id==R.id.exit)
        {
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }

        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private Toolbar toolbar;


        public SectionsPagerAdapter(FragmentManager fm, Toolbar toolbar) {

            super(fm);
            this.toolbar = toolbar;

        }

        @Override
        public Fragment getItem(int position) {

            switch (position)
            {
                case 0:
                    tabLayout.setBackground(new ColorDrawable(Color.parseColor("#00cc00")));
                    return new Dieta();
                case 1: return new Trening();
                case 2: return new Kalkulator();

            }
            return null;
        }

        @Override
        public int getCount() {

            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Dieta";
                case 1:
                    return "Trening";
                case 2:
                    return "Kalkulatory";
            }
            return null;
        }
    }
}