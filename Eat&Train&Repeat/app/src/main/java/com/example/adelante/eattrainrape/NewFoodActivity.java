package com.example.adelante.eattrainrape;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;


public class NewFoodActivity extends AppCompatActivity {

    private Food food;
    private EditText editTextNam;
    private EditText editTextCalories;
    private EditText editTextCarbs;
    private EditText editTextProteins;
    private EditText editTextSugar;
    private EditText editTextFat;
    private EditText editTextKind;
    private EditText editTextKod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        this.food = new Food();
        this.editTextNam = (EditText) findViewById(R.id.editTextNam);
        this.editTextCalories = (EditText) findViewById(R.id.editTextCalories);
        this.editTextCarbs = (EditText) findViewById(R.id.editTextCarbs);
        this.editTextProteins = (EditText) findViewById(R.id.editTextBialko);
        this.editTextSugar = (EditText) findViewById(R.id.editTextSugar);
        this.editTextFat = (EditText) findViewById(R.id.editTextFat);
        this.editTextKind = (EditText) findViewById(R.id.editTextKind);
        this.editTextKod = (EditText) findViewById(R.id.editTextKod);


        Intent intent = getIntent();
        if (intent != null){
            Bundle bundle = intent.getExtras();
            if (bundle != null){
                this.food.setId(bundle.getInt("id"));
                this.editTextNam.setText(bundle.getString("nam"));
                this.editTextCalories.setText(bundle.getString("calories"));
                this.editTextCarbs.setText(bundle.getString("carbs"));
                this.editTextProteins.setText(bundle.getString("proteins"));
                this.editTextSugar.setText(bundle.getString("sugar"));
                this.editTextFat.setText(bundle.getString("fat"));
                this.editTextKind.setText(bundle.getString("kind"));
                this.editTextKod.setText(bundle.getString("barcode"));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_novo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void update (View view)
    {
        this.food.setNames(this.editTextNam.getText().toString());
        this.food.setCalories(this.editTextCalories.getText().toString());
        this.food.setCarbs(this.editTextCarbs.getText().toString());
        this.food.setProteins(this.editTextProteins.getText().toString());
        this.food.setSugar(this.editTextSugar.getText().toString());
        this.food.setFat(this.editTextFat.getText().toString());
        this.food.setKind(this.editTextKind.getText().toString());
        this.food.setBarcode(this.editTextKod.getText().toString());
        this.food.update();

        Toast.makeText(this,this.food.get_message(),Toast.LENGTH_LONG).show();
        if (food.is_status())
            finish();
    }

    public void cancel (View view){
        finish();
    }
}
