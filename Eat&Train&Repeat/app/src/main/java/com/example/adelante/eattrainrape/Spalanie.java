package com.example.adelante.eattrainrape;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class Spalanie extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalanie);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        final Button button_OK = (Button) findViewById(R.id.buttonObliczSpalanie);
        final TextView spaloneKalorie = (TextView) findViewById(R.id.textView14);
        final TextView textCzasSpalanie = (TextView) findViewById(R.id.textCzasSpalanie);
        final TextView textWagaSpalane = (TextView) findViewById(R.id.textWagaSpalanie);
        final Spinner spinnerAktywnosc = (Spinner) findViewById(R.id.spinnerAktywnosc);

        button_OK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String strCzasSp = textCzasSpalanie.getText().toString();
                String strWagaSp = textWagaSpalane.getText().toString();



                if (TextUtils.isEmpty(strWagaSp)) {
                    textWagaSpalane.setError("Uzupełnij!");
                    return;
                }
                else if (TextUtils.isEmpty(strCzasSp)) {
                    textCzasSpalanie.setError("Uzupełnij!");
                    return;
                }

                else{

                    String rodzajAkt = spinnerAktywnosc.getSelectedItem().toString();

                    Double waga = Double.parseDouble(textWagaSpalane.getText().toString());
                    Double czas = Double.parseDouble(textCzasSpalanie.getText().toString());

                    Double spalone = 0.0;


                    if (rodzajAkt.equals("Bieganie (wolno 7,5 km/h)")) {
                        spalone = Math.floor(13.5*(waga/100)*czas);
                    }
                    if (rodzajAkt.equals("Bieganie (szybko 12 km/h)")) {
                        spalone = Math.floor(20*(waga/100)*czas);
                    }
                    if (rodzajAkt.equals("Bieganie (b. szybko 15 km/h)")) {
                        spalone = Math.floor(26.67 * (waga / 100) * czas);
                    }

                    spaloneKalorie.setText("Spalone kalorie : " + spalone.intValue() + " kcal");

                }
            }
        });
    }
}
