package com.example.adelante.eattrainrape;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class Trening extends Fragment {
    private ListView cwiczeniaListView;



    public String getTraining() {
        Dane_treningowe daneTreningowe = new Dane_treningowe();
        Calendar c = Calendar.getInstance();
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        String trening = "";
        // 1 - Niedziela 7 - Sobota
        switch (dayOfWeek) {
            case Calendar.MONDAY:
                trening = daneTreningowe.treningPoniedzialek;
                this.cwiczeniaListView.setAdapter(new TreningAdapter(this.getActivity(),new TreningiLista().getListaPon()));
                break;
            case Calendar.TUESDAY:
                trening = daneTreningowe.treningWtorek;
                this.cwiczeniaListView.setAdapter(new TreningAdapter(this.getActivity(),new TreningiLista().getListaWt()));
                break;
            case Calendar.WEDNESDAY:
                trening = daneTreningowe.treningSroda;
                this.cwiczeniaListView.setAdapter(new TreningAdapter(this.getActivity(),new TreningiLista().getListaSro()));
                break;
            case Calendar.THURSDAY:
                trening = daneTreningowe.treningCzwartek;
                this.cwiczeniaListView.setAdapter(new TreningAdapter(this.getActivity(),new TreningiLista().getListaCzw()));
                break;
            case Calendar.FRIDAY:
                trening = daneTreningowe.treningPiatek;
                this.cwiczeniaListView.setAdapter(new TreningAdapter(this.getActivity(),new TreningiLista().getListaPt()));
                break;
            case Calendar.SATURDAY:
                trening = daneTreningowe.treningSobota;
                this.cwiczeniaListView.setAdapter(new TreningAdapter(this.getActivity(),new TreningiLista().getListaSob()));
                break;
            case Calendar.SUNDAY:
                trening = daneTreningowe.treningNiedziela;
                this.cwiczeniaListView.setAdapter(new TreningAdapter(this.getActivity(),new TreningiLista().getListaNd()));
                break;
        }
        if (trening == "") {
            return "Na dziś nie masz treningu";
        } else {
            return "Dziś robisz: " + trening;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trening_main, container, false);

        final TextView dzienTrening = (TextView) view.findViewById(R.id.dzienTrening);
        this.cwiczeniaListView = (ListView) view.findViewById(R.id.listViewCwiczenia);
        dzienTrening.setText(getTraining());


        return view;
    }

    public void onResume() {
        super.onResume();
        final TextView dzienTrening = (TextView) getView().findViewById(R.id.dzienTrening);
        this.cwiczeniaListView = (ListView) getView().findViewById(R.id.listViewCwiczenia);
        dzienTrening.setText(getTraining());
    }
}