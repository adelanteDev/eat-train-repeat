package com.example.adelante.eattrainrape;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by shaq on 17.01.16.
 */
public class TreningAdapter extends ArrayAdapter<TreningiLista> {

    private Context context;
    private ArrayList<TreningiLista> lista;
    public TreningAdapter(Context context, ArrayList<TreningiLista> lista){
        super(context,0,lista);
        this.context = context;
        this.lista = lista;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TreningiLista itemPosition = this.lista.get(position);

        convertView = LayoutInflater.from(this.context).inflate(R.layout.item_lista_trening, null);
        final View layout = convertView;

        TextView textViewCwiczenie = (TextView) convertView.findViewById(R.id.textViewNam);
        textViewCwiczenie.setText(itemPosition.getNazwa());

        return convertView;
    }
}
