package com.example.adelante.eattrainrape;

import java.util.ArrayList;

/**
 * Created by shaq on 17.01.16.
 */
public class TreningiLista {
    String nazwa;

    public TreningiLista(){}

    public TreningiLista(String stringTreningowy) {
        nazwa = stringTreningowy;
    }
    public String  getNazwa(){
        return nazwa;
    }

    static ArrayList<TreningiLista> treningPoniedzialek = new ArrayList<TreningiLista>();
    static ArrayList<TreningiLista> treningWtorek = new ArrayList<TreningiLista>();
    static ArrayList<TreningiLista> treningSroda = new ArrayList<TreningiLista>();
    static ArrayList<TreningiLista> treningCzwartek = new ArrayList<TreningiLista>();
    static ArrayList<TreningiLista> treningPiatek = new ArrayList<TreningiLista>();
    static ArrayList<TreningiLista> treningSobota = new ArrayList<TreningiLista>();
    static ArrayList<TreningiLista> treningNiedziela = new ArrayList<TreningiLista>();

    public ArrayList<TreningiLista> getListaPon() {
        return treningPoniedzialek;
    }
    public ArrayList<TreningiLista> getListaWt() {
        return treningWtorek;
    }
    public ArrayList<TreningiLista> getListaSro() {
        return treningSroda;
    }
    public ArrayList<TreningiLista> getListaCzw() {
        return treningCzwartek;
    }
    public ArrayList<TreningiLista> getListaPt() {
        return treningPiatek;
    }
    public ArrayList<TreningiLista> getListaSob() {
        return treningSobota;
    }
    public ArrayList<TreningiLista> getListaNd() {
        return treningNiedziela;
    }

}
