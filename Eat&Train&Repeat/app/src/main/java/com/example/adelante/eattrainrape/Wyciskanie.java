package com.example.adelante.eattrainrape;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Wyciskanie extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wyciskanie);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        final Button button_Oblicz = (Button) findViewById(R.id.buttonObliczWyciskanie);
        final TextView obciazenieText = (TextView) findViewById(R.id.textObciazenie);
        final Spinner spinnerIloscPowtorzen = (Spinner) findViewById(R.id.spinnerIloscPowtorzen);
        final TextView maksymalneObciazenie = (TextView) findViewById(R.id.textView16);
        final TextView piepro = (TextView) findViewById(R.id.textView17);
        final TextView szepro = (TextView) findViewById(R.id.textView18);
        final TextView siepro = (TextView) findViewById(R.id.textView19);
        final TextView opro = (TextView) findViewById(R.id.textView20);
        final TextView dziepro = (TextView) findViewById(R.id.textView21);
        button_Oblicz.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String strObciazenie = obciazenieText.getText().toString();

                if (TextUtils.isEmpty(strObciazenie)) {
                    obciazenieText.setError("Uzupełnij!");
                    return;
                }
                else if (Double.parseDouble(strObciazenie)<0.0 || Double.parseDouble(strObciazenie)>500) {
                  obciazenieText.setError("Wpisana wartość wychodzi poza zakres!");
                return;
                }
                else {

                    String rodzajAkty = spinnerIloscPowtorzen.getSelectedItem().toString();
                    Double obciazenie = Double.parseDouble(obciazenieText.getText().toString());

                    Double max_obc = 0.0;
                    if (rodzajAkty.equals("1")) {
                        max_obc = Math.floor(1.0 * obciazenie);
                    }
                    if (rodzajAkty.equals("2")) {
                        max_obc = Math.floor(1.07 * obciazenie);
                    }
                    if (rodzajAkty.equals("3")) {
                        max_obc = Math.floor(1.12 * obciazenie);
                    }
                    if (rodzajAkty.equals("4")) {
                        max_obc = Math.floor(1.15 * obciazenie);
                    }
                    if (rodzajAkty.equals("5")) {
                        max_obc = Math.floor(1.18 * obciazenie);
                    }
                    if (rodzajAkty.equals("6")) {
                        max_obc = Math.floor(1.21 * obciazenie);
                    }
                    if (rodzajAkty.equals("7")) {
                        max_obc = Math.floor(1.24 * obciazenie);
                    }
                    if (rodzajAkty.equals("8")) {
                        max_obc = Math.floor(1.27 * obciazenie);
                    }
                    if (rodzajAkty.equals("9")) {
                        max_obc = Math.floor(1.30 * obciazenie);
                    }
                    if (rodzajAkty.equals("10")) {
                        max_obc = Math.floor(1.33 * obciazenie);
                    }
                    DecimalFormat df = new DecimalFormat("#.##");
                    maksymalneObciazenie.setText("Maksymalne obciążenie : " + df.format(max_obc) + " kg");
                    piepro.setText("50%: " + df.format(max_obc*0.5) + " kg");
                    szepro.setText("60%: " + df.format(max_obc*0.6) + " kg");
                    siepro.setText("70%: " + df.format(max_obc*0.7) + " kg");
                    opro.setText("80%: " + df.format(max_obc*0.8) + " kg");
                    dziepro.setText("90%: " + df.format(max_obc*0.9) + " kg");
                }
            }
        });
    }
}