package com.example.adelante.eattrainrape;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

public class Zjedzone extends AppCompatActivity {
    private ListView listViewFoodSniadanie;
    private ListView listViewFoodObiad;
    private ListView listViewFoodKolacja;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zjedzone);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        this.listViewFoodSniadanie = (ListView) findViewById(R.id.listViewFoodSniadanie);
        this.listViewFoodSniadanie.setAdapter(new ZjedzoneAdapter(this, new Food().getSniadanieLista()));
        this.listViewFoodObiad= (ListView) findViewById(R.id.listViewFoodObiad);
        this.listViewFoodObiad.setAdapter(new ZjedzoneAdapter(this, new Food().getObiadLista()));
        this.listViewFoodKolacja = (ListView) findViewById(R.id.listViewFoodKolacja);
        this.listViewFoodKolacja.setAdapter(new ZjedzoneAdapter(this, new Food().getKolacjaLista()));
    }

}
