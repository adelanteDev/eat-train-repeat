package com.example.adelante.eattrainrape;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class ZjedzoneAdapter extends ArrayAdapter<Food> {
    private Context context;
    private ArrayList<Food> lista;

    public ZjedzoneAdapter(Context context, ArrayList<Food> lista) {
        super(context, 0, lista);
        this.context = context;
        this.lista = lista;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final Food itemPosition = this.lista.get(position);

        convertView = LayoutInflater.from(this.context).inflate(R.layout.zjedzone_item,null);
        final View layout = convertView;

        TextView textView = (TextView) convertView.findViewById(R.id.textViewNam);
        textView.setText(itemPosition.getNames());

        DecimalFormat df = new DecimalFormat("#.#");

        TextView textViewCalories = (TextView) convertView.findViewById(R.id.textViewCalories);
        textViewCalories.setText(df.format(Float.parseFloat(itemPosition.getCalories())) +" kcal");

        TextView textViewCarbs = (TextView) convertView.findViewById(R.id.textViewCarbs);
        textViewCarbs.setText("W: " + df.format(Float.parseFloat(itemPosition.getCarbs())));

        TextView textViewProteins = (TextView) convertView.findViewById(R.id.textViewProteins);
        textViewProteins.setText("  P: " + df.format(Float.parseFloat(itemPosition.getProteins())));

        TextView textViewFats = (TextView) convertView.findViewById(R.id.textViewFats);
        textViewFats.setText("  F: " +  df.format(Float.parseFloat(itemPosition.getFat())));

        Button buttonDelete = (Button)convertView.findViewById(R.id.buttonDelete);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Food.sniadanieLista.remove(itemPosition);
                Food.obiadLista.remove(itemPosition);
                Food.kolacjaLista.remove(itemPosition);

                if (itemPosition._status)
                    layout.setVisibility(View.GONE);
                else
                    Toast.makeText(context, itemPosition.get_message(), Toast.LENGTH_LONG).show();
            }
        });
        return convertView;
    }
}