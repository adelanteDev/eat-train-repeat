# „Eat&Train&Repat ” - aplikacja do monitorowania diety oraz treningu.

### W skrócie :

Aplikacja mobilna dla Android'a służąca do monitorowania diety oraz treningu. Oprocz licznika kalorii oraz mikroskładników posiada również spis zjedzonych aktualnego dnia posiłkow, liczne kalkulatory oraz funkcję treningu. Ważną funkcjonalnością jest czytnik kodów kreskowych, a wszystkie produktu znajdują się w zewnętrznej bazie danych.

### Przykładowe zrzuty ekranu :

![Screenshot_2016-07-23-12-55-10.png](https://bitbucket.org/repo/GGKroa/images/3889026161-Screenshot_2016-07-23-12-55-10.png)

![Screenshot_2016-07-23-12-56-25.png](https://bitbucket.org/repo/GGKroa/images/4058610752-Screenshot_2016-07-23-12-56-25.png)

![Screenshot_2016-07-23-12-56-35.png](https://bitbucket.org/repo/GGKroa/images/1800023217-Screenshot_2016-07-23-12-56-35.png)

![Screenshot_2016-07-23-12-57-36.png](https://bitbucket.org/repo/GGKroa/images/3304620169-Screenshot_2016-07-23-12-57-36.png)

![Screenshot_2016-07-23-12-57-41.png](https://bitbucket.org/repo/GGKroa/images/780032448-Screenshot_2016-07-23-12-57-41.png)

![Screenshot_2016-07-23-12-57-51.png](https://bitbucket.org/repo/GGKroa/images/1281572803-Screenshot_2016-07-23-12-57-51.png)

![Screenshot_2016-07-23-12-58-43.png](https://bitbucket.org/repo/GGKroa/images/296317283-Screenshot_2016-07-23-12-58-43.png)